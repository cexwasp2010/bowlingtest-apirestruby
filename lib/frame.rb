# This module is based in composite design patron
module Frame
  # The Frame class represents one frame of sequence.
  class Frame
    # @return [Frame]
    def parent
      @parent
    end

    # Optionally, the base Frame can declare an interface for setting and
    # accessing a parent of the frame in a tree structure. It can also provide
    # some default implementation for these methods.
    def parent=(parent)
      @parent = parent
    end

    # In some cases, it would be beneficial to define the child-management
    # operations right in the base Frame class. This way, you won't need to
    # expose any concrete frame classes to the client code, even during the
    # object tree assembly. The downside is that these methods will be empty for
    # the leaf-level frames.
    def add(frame)
      raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
    end

    # @abstract
    #
    # @param [Frame] frame
    def remove(frame)
      raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
    end

    # You can provide a method that lets the client code figure out whether a
    # frame can bear children.
    def composite?
      false
    end

    # The base Frame may implement some default behavior or leave it to
    # concrete classes (by declaring the method containing the behavior as
    # "abstract").
    def operation
      raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
    end
  end

  # The GoodRoll class represents the strike and spare that may have children.
  class GoodRoll < Frame
    def initialize
      @children = []
    end

    # @param [Frame] frame
    def add(frame)
      @children.append(frame)
      frame.parent = self
    end

    # @param [Frame] frame
    def remove(frame)
      @children.delete(frame)
      frame.parent = nil
    end

    # @return [Boolean]
    def composite?
      true
    end

    # Return sum
    # This calculate the score for all rolls of the Strike or Spare.
    def operation
      # Variable to contain the sum of children
      sum = 0
      # Iterate the @children array for sum each child
      @children.each{|child| sum += child.operation}
      return sum
    end
  end

  # The Roll class represents a single attempt as roll.
  class Roll < Frame
    attr_reader  :points
    
    def initialize(points)
      # This set the points according to the result of the shot 
      case points
      when "-"
        @points = 0
      when "X"
        @points = 10
      when "/"
        @points = 10
      else
        @points = points.to_i
      end
    end
    
    # Return the points for Roll for your sum
    def operation
      # p @points
      return @points
    end
  end
end