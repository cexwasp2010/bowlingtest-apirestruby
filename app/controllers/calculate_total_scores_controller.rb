class CalculateTotalScoresController < ApplicationController
  before_action :set_calculate_total_score, only: [:show, :update, :destroy]
  before_action :require_jwt

  # GET /calculate_total_scores
  def index
    @calculate_total_scores = CalculateTotalScore.all

    render json: @calculate_total_scores
  end

  # GET /calculate_total_scores/1
  def show
    render json: @calculate_total_score
  end

  # POST /calculate_total_scores
  def create
    @calculate_total_score = CalculateTotalScore.new(calculate_total_score_params)
    
    if @calculate_total_score.save
      render json: @calculate_total_score, status: :created, location: @calculate_total_score
    else
      render json: @calculate_total_score.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /calculate_total_scores/1
  def update
    if @calculate_total_score.update(calculate_total_score_params)
      render json: @calculate_total_score
    else
      render json: @calculate_total_score.errors, status: :unprocessable_entity
    end
  end

  # DELETE /calculate_total_scores/1
  def destroy
    @calculate_total_score.destroy
  end
  
  # Validate the headers for the token authorization  
  def require_jwt
    token = request.headers["HTTP_AUTHORIZATION"]
    if !token
      head :forbidden
    end
    if !valid_token(token)
      head :forbidden
    end
  end

  private
    # Validate that the token is correct for permit access
    def valid_token(token)
      unless token
        return false
      end

      token.gsub!('Bearer ','')
      begin
        decoded_token = JWT.decode token, 's3cr3t', true
        return true
      rescue JWT::DecodeError => e
        Rails.logger.warn "Error decoding the JWT: "+ e.to_s
      end
      false
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_calculate_total_score
      @calculate_total_score = CalculateTotalScore.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def calculate_total_score_params
      params.require(:calculate_total_score).permit(:sequence)
    end
end
