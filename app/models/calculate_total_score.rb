class CalculateTotalScore < ApplicationRecord
	before_create :calculate
	@@points = 0

	# Calculate score when the frame contain a pin or miss.
  # @param [Frame] frame
	def calculate_score(frame)
		@@points += frame.operation
	end

	private
		# this method calculate total score before the Object is created 
		def calculate
			@@points = 0
	    if self.sequence.nil?
	      raise ArgumentError, 'The sequence param is empty. Please set this'
	    else
	    	sequence = self.sequence
				tree = Frame::GoodRoll.new
		    # Iterate the sequence in "seq" field ant index "i"
		    sequence.split('').each_with_index do |seq, i|
					# Check if the roll is a strike or spare
					# else the roll is pin or miss
					if ["X", "/"].include? seq
						# Check if the roll is a strike
						# else the roll is a spare
						if seq == "X"
							# Check that the roll i+2  isn't nil
							unless sequence[i+2].eql? nil
								# Create a new instance of GoodRoll called @strike that is the parent of branch
								@strike = Frame::GoodRoll.new
								# Create and add new Roll instance of current roll with this value "seq" 
								@strike.add(Frame::Roll.new(seq))
								# Check if the roll i+2 is a spare for only add this roll
								# else add the next 2 rolls to the object @strike
								if sequence[i+2] == "/"
									# Set the instance variable @remove_index with the index i+1 because the 
									# i+2 frame contain a spare and is not necesary add this value
		    					@remove_index = i+1
									# Create and add a new Roll instance of i+2 roll with its value
									@strike.add(Frame::Roll.new(sequence[i+2]))
								else
									# Create and add a new Roll instance of next roll with its value
									@strike.add(Frame::Roll.new(sequence[i+1]))
									# Create and add a new Roll instance of i+2 roll with its value
									@strike.add(Frame::Roll.new(sequence[i+2]))
								end
								# Add the object @strike to the principal composite of frames call tree
								tree.add(@strike)
							end
						else
							# Check that item in position i+1 isn't nil for add the next rol value
							unless sequence[i+1].eql? nil
								# Create a new instance of GoodRoll called @spare that is the parent of branch 
								@spare = Frame::GoodRoll.new
								# Check if the item in position i+2 is null for set the instance variable with the index i+1 because the 
								# i+2 frame is nil and not add
								@remove_index = i+1 if sequence[i+2].eql? nil
								# Create and add a new Roll instance of current roll with its value
								@spare.add(Frame::Roll.new(seq))
								# Create and add a new Roll instance of next roll with its value
								@spare.add(Frame::Roll.new(sequence[i+1]))
								# Add the object @spare to the principal composite of frames call tree
								tree.add(@spare)
							end
						end
					# Check if the index not is equal to @remove_index for add roll to branch
			    elsif i != @remove_index
						# Check if the index in the position i+1 isn't a spare for add roll to branch 
						unless sequence[i+1].eql? "/"
							# Create a new instance of Roll called @roll with the value for this roll 
			    		@roll = Frame::Roll.new(seq)
							# Add the object @roll to the principal composite of frames call tree
							tree.add(@roll)
						end
					end
		    end
				# Call the calculate_score function for iterate the tree object and sum its child values 
	    	calculate_score(tree)
			  # p "points: " + @@points.to_s
		    # asssign the points calculated to total_score field for save registry after
		    self.total_score = @@points
	    end
    end
end
