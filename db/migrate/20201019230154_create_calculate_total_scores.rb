class CreateCalculateTotalScores < ActiveRecord::Migration[5.2]
  def change
	  drop_table(:calculate_total_scores, if_exists: true)
    create_table :calculate_total_scores do |t|
      t.json :sequence, null: false
      t.integer :total_score

      t.timestamps
    end
  end
end
