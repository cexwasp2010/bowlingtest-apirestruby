require 'test_helper'

class CalculateTotalScoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @calculate_total_score = calculate_total_scores(:four)
  end

  test "should create calculate_total_score" do
    assert_difference('CalculateTotalScore.count') do
      post calculate_total_scores_url, headers: { "HTTP_AUTHORIZATION" => "Bearer " + build_jwt }, params: { calculate_total_score: { sequence: 'X2/3--1-931724/XXXX'} }, as: :json
    end

    assert_response 201
  end

  test "should show calculate_total_score" do
    get calculate_total_score_url(@calculate_total_score), headers: { "HTTP_AUTHORIZATION" => "Bearer " + build_jwt }, as: :json, as: :json
    assert_response :success
  end

  test "should update calculate_total_score" do
    patch calculate_total_score_url(@calculate_total_score), headers: { "HTTP_AUTHORIZATION" => "Bearer " + build_jwt }, as: :json, params: { calculate_total_score: { sequence: @calculate_total_score.sequence, total_score: @calculate_total_score.total_score } }, as: :json
    assert_response 200
  end

  test "should destroy calculate_total_score" do
    assert_difference('CalculateTotalScore.count', -1) do
      delete calculate_total_score_url(@calculate_total_score), headers: { "HTTP_AUTHORIZATION" => "Bearer " + build_jwt }, as: :json
    end

    assert_response 204
  end

  # Security tests
  test "can' get calculate_total_score with no auth" do
    get calculate_total_scores_url, as: :json
    assert_response :forbidden
  end

  test "can get calculate_total_score with header" do
    get calculate_total_scores_url, headers: { "HTTP_AUTHORIZATION" => "Bearer " + build_jwt }, as: :json
    assert_response :success
  end

  test "expired jwt fails" do
    get calculate_total_scores_url, headers: { "HTTP_AUTHORIZATION" => "Bearer " + build_jwt(-1) }, as: :json
    assert_response :forbidden
  end

  test "can get calculate_total_score content" do
    get calculate_total_score_url(@calculate_total_score), headers: { "HTTP_AUTHORIZATION" => "Bearer " + build_jwt }, as: :json
    assert_response 200
  end
  # Generate random key for http headers
  # The default for test in postman is "key":"AUTHORIZATION", "value":"Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJmdXNpb25hdXRoLmlvIiwiYXVkIjoiMjM4ZDQ3OTMtNzBkZS00MTgzLTk3MDctNDhlZDhlY2QxOWQ5Iiwic3ViIjoiMTkwMTZiNzMtM2ZmYS00YjI2LTgwZDgtYWE5Mjg3NzM4Njc3IiwibmFtZSI6IkNlc2FyIENvcmRvYmEiLCJyb2xlcyI6WyJVU0VSIl19.ZHw5jhWfq4R8TT2UzWc4pVdPmPrs4W_oBz1FlaBiYeY"
  def build_jwt(valid_for_minutes = 5)
    # exp = Time.now.to_i + (valid_for_minutes*60)
    payload = { "iss": "fusionauth.io",
                # "exp": exp,
                "aud": "238d4793-70de-4183-9707-48ed8ecd19d9",
                "sub": "19016b73-3ffa-4b26-80d8-aa9287738677",
                "name": "Cesar Cordoba",
                "roles": ["USER"]
    }

    JWT.encode payload, 's3cr3t', 'HS256'

  end

end
