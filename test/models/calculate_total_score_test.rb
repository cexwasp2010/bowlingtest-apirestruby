require 'test_helper'

class CalculateTotalScoreTest < ActiveSupport::TestCase

  test "should raise ArgumentError for params empty or nil in CalculateTotalScore" do
	  excep = assert_raises(ArgumentError) { CalculateTotalScore.create }
	  assert_equal("The sequence param is empty. Please set this", excep.message)
	end
  
  test "should not permit total score like nil in calculate total score" do
	  total_score = CalculateTotalScore.create(sequence: 'XXXXXXXXXXXX')
	  assert_not_nil total_score.total_score, "Check the total score not nil in total calculate score"
	end

  test "should check the object calculate total score is an instace of CalculateTotalScore" do
	  total_score = CalculateTotalScore.new
	  assert_instance_of CalculateTotalScore, total_score, "Check total calculate score object is instance of CalculateTotalScore"
	end

  test "show total_score of fourth insertion" do
	  assert_equal calculate_total_scores(:four).total_score, 139, "The total score is not equal for fourth insertion"
	end

end
