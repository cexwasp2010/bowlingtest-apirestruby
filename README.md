# README

This application was development in Bogotá, Colombia at 2020-10-20 by Cesar Augusto Córdoba Bermúdez, Engineer System.

The email for contact is: cesar.warrior@hotmail.com
The Whatsapp is: +57 3003746786

The code source is alocated in: https://gitlab.com/cexwasp2010/bowlingtest-apirestruby/-/tree/master/

* ...
Especifications

In the lib/frame.rb file you find the code of problem world with composite design patron.
For the fuction were create three classes with the Frame, Roll and GoodRoll structure and call before created Activerecord of CalculateTotalScore model in app/models/calculate_total_score.rb file. 

The project use the sqlite3 db for development and test environments and only one table was created for inserte the sequence and total escore for request.

The version of Ruby is ruby 2.4.9p362 (2019-10-02 revision 67824) [x86_64-linux]

The version of Ruby on Rails is Rails 5.2.4.4 

* ...

For create database run:

rake db:migrate

* ...

For run tests:

* Controller:

rails test -b test/controllers/calculate_total_scores_controller_test.rb 

* Model:

rails test test/models/calculate_total_score_test.rb 
* ...

For test with postman:

1. Got to the root project and run rails server with command: "rails s"

2. In one API client add for all request

* Headers: 

"key":"AUTHORIZATION", "value":"Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJmdXNpb25hdXRoLmlvIiwiYXVkIjoiMjM4ZDQ3OTMtNzBkZS00MTgzLTk3MDctNDhlZDhlY2QxOWQ5Iiwic3ViIjoiMTkwMTZiNzMtM2ZmYS00YjI2LTgwZDgtYWE5Mjg3NzM4Njc3IiwibmFtZSI6IkNlc2FyIENvcmRvYmEiLCJyb2xlcyI6WyJVU0VSIl19.ZHw5jhWfq4R8TT2UzWc4pVdPmPrs4W_oBz1FlaBiYeY"

3. In one API client add

* For get request:

"url": localhost:3000/calculate_total_scores

* For post request:

"url": localhost:3000/calculate_total_scores
body:

{
"sequence": "valid sequence of bowling"
}

The API return one json with the attributes of item created and the score of game.

